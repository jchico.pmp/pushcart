<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index(Category $category) {
    	$this->authorize('create', $category);
        $categories = Category::all();
    	return view('categories.index')->with('categories',$categories);

    }

   /* OTHER SOLUTION
    public function show($category) {
    	
    	$result = Category::find($category);
    	return view('categories.show')->with('category', $result);
    }*/


    public function show(Category $category) {
    	
    	$this->authorize('create', $category);
    	return view('categories.show')->with('category', $category);
    }


    public function create(Category $category) {
    	
        $this->authorize('create', $category);
        return view('categories.create');
    }

    public function store(Request $request, Category $category) {
    	//dd($request->input('name')); check die and dump
    	$this->authorize('create', $category);
        $request->validate([
    			'name' => 'string|required|max:50|unique:categories,name'
    	]);
    	$name = $request->input('name');

    	$category = new Category;
    	$category->name = $name;
    	$category->save();
    	return redirect(route('categories.index'));
    }

    public function edit(Category $category){

    	$this->authorize('create', $category);
        $result = Category::find($category);
    	return view('categories.edit')->with('category',$result);
    }

    public function update(Category $category, Request $request){

        $this->authorize('create', $category);
    	$request->validate([
    			'name' => 'string|required|max:50|unique:categories,name'
    	]);

    	$category->name = $request->input('name');

    	//$result = Category::find($category);

    	//$result->name = $request->input('name');
    	
    	//$result->save();

    	$category->save();
    	return redirect(route("categories.show",['category'=> $category->id]));
    }

   /* OTHER SOLUTION public function destroy($category){

    	$result = Category::find($category);
    	$result->delete();
    	return "destroy method";
    }*/ 

    public function destroy(Category $category){

    	$this->authorize('create', $category);
        $category->delete();
    	return redirect(route('categories.index'));
    }

}

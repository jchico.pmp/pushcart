<?php

namespace App\Http\Controllers;

use App\Transaction;
Use App\Product;
Use App\Status;
use Illuminate\Http\Request;
use Str;
use Auth;
use Session;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Transaction $transaction)
    {
        // dd($transaction->id);
        if(Auth::user()->role_id ==1){
            $transactions = Transaction::all();
        } else {
            $transactions = Transaction::all()->whereIn('user_id', Auth::user()->id);
        }
        return view('transactions.index')->with('transaction',$transactions)->with('statuses',Status::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*TRANSACTION TABLE
        transaction code
        **status_id //gawa na daw kasi naglagay ng ->default(1)
        **payment_mode_id //gawa na daw kasi naglagay ng ->default(1) sa create transactions table
        user_id
        total
        */

        /* pivot table
        product_id
        transaction_id
        quantity
        subtotal


        */

        $transaction = new Transaction;
        $transaction->user_id = Auth::user()->id;
        $transaction->transaction_code = Auth::user()->id.Str::random(10);


        $transaction->save();

        // dd($transaction->id);

        $product_ids = array_keys(Session::get('cart'));


        $products = Product::find($product_ids);

        $total = 0;
        foreach ($products as $product) {
            $product->quantity = Session::get("cart.$product->id");
            $product->subtotal = $product->price * $product->quantity;
            $total += $product->subtotal;

            $transaction->products()
            ->attach(
                $product->id,
                ["quantity" => $product->quantity,
                    "subtotal" => $product->subtotal,
                    "price" => $product->price
                ]);
        }

        $transaction->total = $total;
        $transaction->save();

        Session::forget('cart');

        return redirect( route('transactions.show', ['transaction' => $transaction->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        $this->authorize('view', $transaction);
        $statuses = Status::all();
        return view('transactions.show')->with('transaction',$transaction)->with('statuses',$statuses);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        $transaction->status_id = $request->status;
        $transaction->save();
        return redirect( route('transactions.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }

    public function create_paypal_payment(Request $request) {
        if(!Session::has('cart')){
            return ['message' => "Session cart not found"];
        }

        $data = json_decode($request->getContent(),true);
        $transaction = new Transaction;
        $transaction->user_id = Auth::user()->id;
        $transaction->transaction_code = $data['transactionCode'];
        // $transaction->transaction_code = "TRANSACTIONCODE_1";


        $transaction->save();
        // dd('dito');

        // dd($transaction->id);

        $product_ids = array_keys(Session::get('cart'));


        $products = Product::find($product_ids);

        $total = 0;
        foreach ($products as $product) {
            $product->quantity = Session::get("cart.$product->id");
            $product->subtotal = $product->price * $product->quantity;
            $total += $product->subtotal;

            $transaction->products()
            ->attach(
                $product->id,
                ["quantity" => $product->quantity,
                    "subtotal" => $product->subtotal,
                    "price" => $product->price
                ]);
        }

        $transaction->total = $total;
        $transaction->save();

        Session::forget('cart');

        
        return ["transaction_id" => $transaction->id];
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
        		'name'=> 'Patayin sa Sindak si Mikee Valle',
        		'price'=>'650',
        		'description'=> 'Ang horror movie ng taon!',
        		'category_id'=> 1,
        		'image'=> 'products/47vKHxqkjOmTyo2TBEwpXjsn0rHDs1HmsuVUynzI.jpeg'
        ]);

        DB::table('products')->insert([
        		'name'=> 'Ang Magandang Buhay ni Sean Ralph',
        		'price'=>'500',
        		'description'=> 'Ma-drama ito sobra',
        		'category_id'=> 2,
        		'image'=> 'products/N7MzSPuvygQqODtXHfyr5utxXuYhH47p2uvnBSdi.jpeg'
        ]);

        DB::table('products')->insert([
        		'name'=> 'Lolo vs Dada: The Revenge Sequel',
        		'price'=>'300',
        		'description'=> 'Ang malupit na bakbakan ng 2020!',
        		'category_id'=> 3,
        		'image'=> 'products/C1pmLpvoZ80raBwf2HQsMRtyZolNVcyg5C5L0zlV.jpeg'
        ]);
    }
}

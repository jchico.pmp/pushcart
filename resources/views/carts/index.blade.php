@extends('layouts.app')

@section('content')

 {{-- {{ dd(Session::get('cart')) }} --}}
	<div class="container">
		<div class="row">



			<div class="col-12">
				<h3>My Cart</h3>
				@include('products.includes.error-status')
			</div>
			
			@if(Session::has('status'))
					<div class="col-12">
						<div class="alert alert-warning">
							{{ Session::get('status') }}
						</div>
					</div>
			@endif



			@if(Session::has('cart'))
			<div class="col-12">

				{{-- TABLE STARTS HERE --}}
				<div class="table-responsive">
					<table class="table table-striped table-hover text-center">
						<thead>
							<th scope="col">Name</th>
							<th scope="col">Price per Unit</th>
							<th scope="col">Quantity</th>
							<th scope="col">Subtotal</th>
							<th scope="col">Action</th>
						</thead>

						<tbody>
							@foreach($products as $product)
							{{-- START OF ROW --}}
								<tr>
									{{-- name --}}
									<th scope="row">
									{{$product->name}}
								</th>
									{{-- price --}}
									<td>
										&#8369; <span>{{ number_format($product->price,2) }}</span>
									</td>
									{{-- quantity --}}
									<td>
										<div class="add-tocart-field mb-1">
											<form action="{{ route('carts.update',['cart' => $product->id])}}" method="POST">
												@csrf
												@method('PUT')

												<input type="number" name="quantity" id="quantity" class="form-control mb-2 w-100" value="{{$product->quantity}}" min="1">
												<button class="btn btn-outline-secondary w-100" type="submit">Edit</button>
											</form>
										</div>
									</td>
									{{-- subtotal --}}
									<td>
										&#8369; <span>{{number_format($product->subtotal,2)}}</span>
									</td>
									{{-- action --}}
									<td>
										<form action="{{ route('carts.destroy', ['cart'=> $product->id])}}" method="POST">
											@csrf
											@method('DELETE')
											<button class="btn btn-danger w-100">
												Remove from Cart
											</button>
										</form>
									</td>
								</tr>
							{{-- END OF ROW --}}
							
							@endforeach

						</tbody>
						<tfoot>
							<tr>
								<td colspan="3" class="text-right bg-secondary text-light">
								Total
							</td>
							<td class="bg-secondary text-light">
										&#8369; <span id="total">{{number_format($total,2)}}</span>
							</td>
							{{-- action --}}
									<td class="bg-secondary">
										
										@can('isLogged')
										<form action="{{ route('transactions.store') }}" method="POST">
											@csrf
											<button class="btn btn-primary w-100 mb-3">
												Checkout
											</button>
										</form>
										<div id="paypal-btn">
											
										</div>

										@endcan
										@cannot('isLogged')
											<a href="{{ route('login') }}" class="btn btn-success w-100">Please Login to Continue</a>
										@endcannot

									</td>
							</tr>
						</tfoot>
					</table>
					{{-- END OF TABLE --}}
				<form action="{{ route('carts.empty')}}" method="POST">
					@csrf
					@method('DELETE')
				<button type="submit" class="btn btn-outline-danger">
												Clear Cart
											</button>
				</form>							
				</div>
			</div>
			@else
					<div class="col-12">
						<div class="alert alert-info text-center">
							<h1>&#128544; Your Cart is Empty!!! &#128544;</h1>
						</div>
					</div>
			@endif
		</div>
	</div>

<script src="https://www.paypal.com/sdk/js?client-id=AaR7pFBDf38hOkFkwVWOip418KchDWejh2Mj7CobbcgWx8G_xAVKdMGrKDnO1bw8BwAW23uSVOEySnXn"></script>
<script>
	paypal.Buttons({
	createOrder: function(data, actions) {
      // This function sets up the details of the transaction, including the amount and line item details.
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: {{ isset($total) ? $total : "0" }}
          }
        }]
      });
    },

  onApprove: function(data, actions) {
    // This function captures the funds from the transaction.
    return actions.order.capture().then(function(details) {
      // This function shows a transaction success message to your buyer.
      alert('Transaction completed by ' + data.orderID);

      let csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

      let transaction = { transactionCode : data.orderID };

      fetch("{{ route('transactions.paypal') }}", {
      		method: 'post',
      		body: JSON.stringify(transaction),
      		headers: {'X-CSRF-TOKEN': csrfToken}
      	})
      .then(response =>response.json())
      .then(res =>{
        	window.location.href = "/transactions/" + res.transaction_id
      });
    })
  }
}).render('#paypal-btn');
</script>
@endsection
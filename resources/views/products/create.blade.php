@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-12 col-md-8 mx-auto">
				<h3>Create Product Form</h3>
				<hr>
				<form class="form-group" action="{{ route('products.store')}}" method="POST" enctype="multipart/form-data">
					@csrf

					@if($errors->has('name'))
						<div class="alert alert-danger">
							{{$errors->first('name')}}
						</div>
					@endif

					<input type="text" name="name" id="name" class="form-control mb-2" placeholder="product name here" value="{{ old('name')}}">

					@if($errors->has('price'))
						<div class="alert alert-danger">
							{{$errors->first('price')}}
						</div>
					@endif

					<input type="text" name="price" id="price" class="form-control mb-2" placeholder="product price" value="{{ old('price')}}">
					
					

					<select name="category-id" id="category-id" class="custom-select">
						@foreach($categories as $category)
							<option
								value="{{$category->id}}"
								{{ old('category-id') == $category->id ? "selected" : ""}}
								>
								{{$category->name}}
							</option>
						@endforeach	
					</select>
					
					@if($errors->has('image'))
						<div class="alert alert-danger">
							{{$errors->first('image')}}
						</div>
					@endif

					<input type="file" name="image" id="image" class="form-control-file mb-2">
					
					@if($errors->has('description'))
						<div class="alert alert-danger">
							{{$errors->first('description')}}
						</div>
					@endif

					<textarea name="description" id="description" cols="10" rows="10" class="form-control mb-2" placeholder="product description">{{ old('description')}}</textarea>

					<button type="submit" class="btn btn-danger mb-2 mx-auto">Create New Product</button>
				</form>
			</div>
		</div>
	</div>
							
@endsection
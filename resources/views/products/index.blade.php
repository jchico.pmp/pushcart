@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-12">
			<form action="" method="get">
				
				<div class="row">
					<div class="col">
						<select name="category" id="category" class="form-control">
							<option value="">All</option>
							@foreach($categories as $category)
							<option value="{{$category->id}}">{{$category->name}}</option>
							@endforeach
						</select>
					</div>
					<div class="col">
				<button class="btn btn-outline-primary">
					Filter
				</button>
						
					</div>
				</div>
				
			</form>
		</div>
	</div>
	@if(Session::has('status'))
		<div class="alert alert-success">
			{{Session::get('status')}}
		</div>

	@endif
	@include('products.includes.error-status')
	<div class="row">


	@foreach($products as $product)
		{{-- START OF CARDS --}}
		<div class="col-12 col-md-4 col-lg-3 pb-3">
			@include('products.includes.product-card')
		</div>
		{{-- END OF CARD --}}
	@endforeach
	</div>
</div>

@endsection
@extends('layouts.app')

@section('content')

<div class="col-12 px-3 text-right"><h4><a href="{{ route('products.index')}}">Back to Products</a></h4>
	</div>

@include('products.includes.error-status')

{{-- START OF CARDS --}}
		<div class="col-12 col-md-4 col-lg-3 pb-3">
			@include('products.includes.product-card')
		</div>
		{{-- END OF CARD --}}



@endsection
<div class="col-12">
			{{-- TABLE START --}}
			<div class="table-responsive">

				{{-- transaction table start --}}
				<table class="table table-striped">
					<tbody>
						<tr>
							{{-- username --}}
							<td>Customer Name:</td>
							<td>{{$transaction->user->email}}</td>
						</tr>
							{{-- transaction code --}}
						<tr>
							<td>Transaction Code:</td>
							<td>{{$transaction->transaction_code}}</td>
						</tr>
						{{-- payment_mode --}}
						<tr>
							<td>Payment Mode:</td>
							<td>{{$transaction->payment_mode->name}}</td>
						</tr>
						{{-- date of purchase --}}
						<tr>
							<td>Date of Purchase:</td>
							<td>{{$transaction->created_at->format('F d, Y')}}</td>
						</tr>
						{{-- status --}}
						<tr>
							<td>Status:</td>
							<td>{{-- {{$transaction->status->name}} --}}
											<form action="{{ route('transactions.update', ['transaction'=>$transaction->id])}}" method="POST">
												@csrf
												@method('PUT')
											<select name="status" id="" class="form form-control">
												@foreach($statuses as $status)
												<option value="{{$status->id}}" 
													{{$transaction->status_id == $status->id ? "selected" : ""}}
													>
													{{$status->name}}
												</option>
												@endforeach
											</select>
												<button class="btn btn-warning">Edit Status</button>
												</form>

											</select>
							</td>
						</tr>
					</tbody>
					{{-- transaction table end --}}
				</table>

				

			</div>
			{{-- TABLE END --}}
		</div>
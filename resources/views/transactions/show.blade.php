@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3 class="text-center">
					Transaction
				</h3>
				<hr>
			</div>
		</div>

		@include('transactions.includes.transaction')

<div class="table-responsive">
		{{-- product_transaction table start --}}
				<table class="table table-striped table-hover">
					<thead>
						<th scope="row">Product Name</th>
						<th scope="row">Price</th>
						<th scope="row">Quantity</th>
						<th scope="row">Subtotal</th>
					</thead>

					<tbody>
						{{-- product_transaction details start --}}
							
						@foreach($transaction->products as $transaction_product)
							<tr>
								<td>
									{{$transaction_product->name}}
								</td>
								<td>
									&#8369; {{ number_format($transaction_product->pivot->price,2)}}
								</td>
								<td>
									{{$transaction_product->pivot->quantity}}
								</td>
								<td>
									&#8369; {{ number_format($transaction_product->pivot->subtotal,2)}}
								</td>
							</tr>

							@endforeach
						{{-- product_transaction details start --}}


					</tbody>
					<tfoot>
						<td class="text-right" colspan="3"><strong>Total</strong></td>
						<td >&#8369; {{ number_format($transaction->total,2)}}</td>

					</tfoot>
				</table>

				{{-- product_transaction table start --}}
		
	</div>
	</div>

@endsection